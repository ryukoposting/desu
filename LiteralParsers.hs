module LiteralParsers (
    intlit, strlit, floatlit, charlit, anylit
) where

import Tokenizer
import ParseTok

lit :: (Token -> Maybe a) -> (a -> Expr) -> ParseTok Expr
lit t e = PT (\ts -> case ts of
                        [] -> basicErr "Expected expression"
                        (x:xs) -> case t x of
                                        Just s -> Ok (e s, xs)
                                        Nothing -> parseErr x "Expected expression")

strlit = lit (\t -> case t of
                         Token _ (StrLit a) -> Just a
                         _ -> Nothing) ExprLitStr
intlit = lit (\t -> case t of
                         Token _ (IntLit a) -> Just a
                         _ -> Nothing) ExprLitInt
floatlit = lit (\t -> case t of
                         Token _ (FloatLit a) -> Just a
                         _ -> Nothing) ExprLitFloat
charlit = lit (\t -> case t of
                         Token _ (CharLit a) -> Just a
                         _ -> Nothing) ExprLitChar

anylit :: ParseTok Expr
anylit = do
    o <- strlit <|> intlit <|> floatlit <|> charlit
    return o
