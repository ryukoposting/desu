module ErrMsg (
    ErrMsg(..)
) where
import Tokenizer

data ErrMsg = ParseError (Token) (String)
            | BasicError (String)

instance Show ErrMsg where
    show (ParseError (Token lno tk) s) = "Line " ++ (show lno) ++
                                            ": Error on token '" ++ (show tk) ++ "': " ++ s
    show (BasicError s) = "Syntax error: " ++ s
