module Utils (
    splitMatching, elemr
) where

-- split a list as soon as the stack is length zero.
-- stack starts at size i, pushes on up, pops on dn
splitMatching :: Eq a => Integer -> a -> a -> [a] -> ([a], [a])
splitMatching _ _ _ [] = ([],[])
splitMatching i up dn (x:xs)
    | x == up   = combine [x] $ splitMatching (i + 1) up dn xs
    | x == dn   = if i == 1
                     then ([x],xs)
                     else combine [x] $ splitMatching (i - 1) up dn xs
    | otherwise = combine [x] $ splitMatching i up dn xs
    where combine = \a (x,y) -> (a ++ x, y)

-- same as builtin elem, except arguments are flipped
elemr :: (Foldable t, Eq a) => t a -> a -> Bool
elemr xf c = snd $ foldl (\(x,z) y-> (x, z || (x == y))) (c, False) xf

