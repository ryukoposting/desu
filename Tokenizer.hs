-- preprocessor... preprocesses (probably can't do in multithreaded)
-- lexer kills off comments and basically makes everything look like pascal (definitely can do per-module)
-- is this where we resolve generics? (definitely multithreaded, if even possible)
-- parser turns it into per-module ASTs (multithreaded per-file)
-- AST is used for optimizations (multithreaded per-file)
-- Combine ASTs (single thread)
-- Optimize again (single thread)
-- Generate LLVM IR (single thread)

module Tokenizer (
    Token(..),
    TokenKey(..),
    tokenizeModule
) where

import Control.Monad (void)
import Data.List (intercalate)
import Data.List.Split (splitOn)
import Data.Char (isLetter, isDigit, isAlphaNum)
import Data.String.Utils
import Numeric (readDec, readOct, readHex, readInt, readFloat)

type LineNum = Integer

data Token = Token (LineNum) (TokenKey) deriving Show

data TokenKey
    -- keywords and module begin/end flags
    = BeginMod
    | EndMod
    | Fun
    | Var
    | Let
    | Return
    | Export
    | Struct
    | Union
    | Void
    | Null
    | If
    | Else
    | While
    | For
    | As
    | ReservedKeyword       -- for reserved, but not implemented, keywords
    -- binary operators
    | Dot
    | DotDot
    | AndAnd
    | AndEq
    | Or
    | OrOr
    | OrEq
    | Carrot
    | CarrotEq
    | Plus
    | PlusEq
    | Div
    | DivEq
    | Minus
    | MinusEq
    | Equ
    | NEq
    | EqEq
    | Mod
    | ModEq
    | MultEq
    | Gt
    | Lt
    | GtEq
    | LtEq
    | GtGt
    | LtLt
    | GtGtEq
    | LtLtEq
    | RightArrow
    | DoubleRightArrow
    | Tilde
    | Question
    | Bang
    | BangBang
    | ColonColon
    -- context-dependent
    | And            -- unary op reference, binary op multiply
    | Star           -- unary op dereference or pointer, binary op multiply
    -- primitive types
    | PrimI8
    | PrimI16
    | PrimI32
    | PrimI64
    | PrimU8
    | PrimU16
    | PrimU32
    | PrimU64
    | PrimF32
    | PrimF64
    | PrimInt
    | PrimUint
    -- identifiers, literals, etc
    | Id (String)
    | StrLit (String)
    | IntLit (Integer)
    | FloatLit (Float)
    | CharLit (Char)
    -- misc
    | Comment
    | OpenCurly
    | CloseCurly
    | OpenParen
    | CloseParen
    | OpenSquare
    | CloseSquare
    | Colon
    | SemiColon
    | Comma
    | MacroIfdef
    deriving (Show, Eq)

type TokenParse = (Token, String)
type ParseRule = String -> Integer -> Maybe TokenParse

alpha = ['a'..'z'] ++ ['A'..'Z']
alphanum = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']
numerical = ['0'..'9']
integral = numerical ++ "-"
floating = integral ++ "."
kwchars = alphanum ++ "_"

-- delim -> esc -> in -> Maybe out(before,after)
-- NOTE: if this doesn't find an end quote, it will tack one on at the end of the input string
splitFirstEsc :: Eq a => a -> a -> [a] -> ([a], [a])
splitFirstEsc _ _ [] = ([],[])
splitFirstEsc d e i = case splitFirst d i of
                           ([],xs) -> ([],xs) -- nothing before the delim, so it can't possibly be escaped
                           (xa,xs) -> if last xa /= e
                                         then (xa,xs)
                                         else combine d xa $ splitFirstEsc d e xs
    where combine = \d aa (ba,bb) -> (aa ++ [d] ++ ba, bb)
                                         
readBin = readInt 2 (\x -> elem x "10") (\x -> case x of 
                                                    '1' -> 1
                                                    '0' -> 0)

newlineCount :: String -> Integer
newlineCount s = foldr (\c n -> if c == '\n' then n + 1 else n) 0 s

-- delim -> in -> out(before,after)
splitFirst :: Eq a => a -> [a] -> ([a], [a])
splitFirst x =
    fmap (drop 1) . break (x ==)

-- delim -> in -> out(before,after)
splitFirstStr :: Eq a => [a] -> [a] -> ([a], [a])
splitFirstStr ds ss = (x, concat xs)
    where (x:xs) = splitOn ds ss

splitAllIn :: Eq a => [a] -> [a] -> [a]
splitAllIn _ [] = []
splitAllIn as (x:xs) = if elem x as
                          then [x] ++ (splitAllIn as xs)
                          else []

-- expects s, doesn't care about subsequent chars.
symbolRule :: String -> TokenKey -> String -> Integer -> Maybe TokenParse
symbolRule _ _ [] _ = Nothing
symbolRule s t cs n
    | startswith s cs = Just (Token n t, drop (length s) cs)
    | otherwise = Nothing

-- expects s followed by a non-keyword character.
keywordRule :: String -> TokenKey -> String -> Integer -> Maybe TokenParse
keywordRule _ _ [] _ = Nothing
keywordRule s t cs n
    | startswith s cs = if not (elem (head rs) kwchars)
                           then Just (Token n t, rs)
                           else Nothing
    | otherwise = Nothing
    where kw = take (length s - 1) cs
          rs = drop (length s) cs

newLineRule :: String -> Integer -> Maybe TokenParse
newLineRule [] _ = Nothing
newLineRule (c:cs) n
    | c == '\n' = Just (Token (n + 1) Comment, cs)
    | otherwise = Nothing

-- rule for primitive type signatures. Identical to keyword rule, for now at least.
primTypeRule :: String -> TokenKey -> String -> Integer -> Maybe TokenParse
primTypeRule = keywordRule

-- expects s followed by a non-keyword character.
macroRule :: String -> TokenKey -> String -> Integer -> Maybe TokenParse
macroRule s = keywordRule ('@':s)

-- rule for token that starts with s and ends with e. discards contents.
wrapRule :: String -> Char -> TokenKey -> String -> Integer -> Maybe TokenParse
wrapRule _ _ _ [] _ = Nothing
wrapRule s e t cs n
    | startswith s cs = Just (Token (newlineCount xa + n + nle) t, xs)
    | otherwise = Nothing
    where (xa, xs) = splitFirst e cs
          nle = if e == '\n' then 1 else 0

-- just like wrapRule, but takes a string as the ending delimiter instead of char.
wrapRuleS :: String -> String -> TokenKey -> String -> Integer -> Maybe TokenParse
wrapRuleS _ _ _ [] _ = Nothing
wrapRuleS s e t cs n
    | startswith s cs = Just (Token (newlineCount xa + n) t, xs)
    | otherwise = Nothing
    where (xa, xs) = splitFirstStr e cs

stringRule :: String -> Integer -> Maybe TokenParse
stringRule [] _ = Nothing
stringRule cs n     -- length check arises due to note for splitFirstEsc
    | startswith "\"" cs = if length xa < (length cs - 1) 
                              then Just (Token (newlineCount xa + n) $ StrLit xa, xs) 
                              else Nothing
    | otherwise = Nothing
    where (xa,xs) = splitFirstEsc '\"' '\\' $ drop 1 cs

charRule :: String -> Integer -> Maybe TokenParse
charRule [] _ = Nothing
charRule (c:d:e:cs) n
    | c == '\'' = if d /= '\\'
                     then (if e == '\''          
                                then Just (Token n $ CharLit d, cs) 
                                else Nothing)
                     else (if startswith "\'" cs 
                                then Just (Token n $ CharLit e, drop 1 cs)
                                else Nothing)
    | otherwise = Nothing
charRule els _ = Nothing

identifierRule :: String -> Integer -> Maybe TokenParse
identifierRule [] _ = Nothing
identifierRule (c:cs) n
    | elem c (alpha ++ "_") = Just (Token n $ Id spl, drop (length spl - 1) cs)
    | otherwise             = Nothing
    where spl = splitAllIn (alphanum ++ "_") (c:cs)

intRule :: String -> Integer -> Maybe TokenParse
intRule [] _ = Nothing
intRule cs ln
    | flt = Nothing
    | startswith "0x" spl = case readHex $ drop 2 ns of
                                 [] -> Nothing
                                 [(n,_)] -> Just (Token ln $ IntLit (n), csr)
    | startswith "0b" spl = case readBin $ drop 2 ns of
                                 [] -> Nothing
                                 [(n,_)] -> Just (Token ln $ IntLit (n), csr)
    | startswith "0o" spl = case readOct $ drop 2 ns of
                                 [] -> Nothing
                                 [(n,_)] -> Just (Token ln $ IntLit (n), csr)
    | otherwise = case readDec ns of
                       [] -> Nothing
                       [(n,_)] -> Just (Token ln $ IntLit (n), csr)
    where spl = splitAllIn (numerical ++ "_oxbABCDEF") cs
          csr = drop (length spl) cs
          flt = startswith "." csr
          ns = filter (/= '_') spl

floatRule :: String -> Integer -> Maybe TokenParse
floatRule [] _ = Nothing
floatRule cs ln
    | bad = Nothing
    | all (\c -> not $ startswith c cs) ["0x", "0b", "0o"] = 
        (case readFloat spl of
                [] -> Nothing
                [(n,_)] -> Just (Token ln $ FloatLit (n), csr))
    | otherwise = Nothing
    where spl = filter (/= '_') $ splitAllIn (numerical ++ "._") cs
          csr = drop (length spl) cs
          bad = case take 1 csr >>= (\x -> [elem x (alphanum)]) of
                     [] -> False
                     (x:xs) -> x

-- arranged in order of precedence, highest to lowest
rules_round1 :: [ParseRule]
rules_round1 = [
-- comments must be highest precedence to guarantee their contents are ignored by the parser
        wrapRuleS   "/*" "*/"   Comment,
        wrapRule    "//" '\n'   Comment,
-- strings and chars next, bc their contents are not parsed either
        stringRule,
        charRule,
        intRule,
        floatRule,
        macroRule   "ifdef"     MacroIfdef,
-- keywords have special rules regarding what characters they're followed by, so they're next
        keywordRule "return"    Return,
        keywordRule "struct"    Struct,
        keywordRule "var"       Var,
        keywordRule "let"       Let,
--         keywordRule "ref"       Ref,
--         keywordRule "mut"       Mut,
        keywordRule "fun"       Fun,
        keywordRule "for"       For,
        keywordRule "while"     While,
        keywordRule "if"        If,
        keywordRule "else"      Else,
        keywordRule "export"    Export,
        keywordRule "union"     Union,
        keywordRule "as"        As,
        keywordRule "null"      Null,
        keywordRule "do"        ReservedKeyword,
        keywordRule "enum"      ReservedKeyword,
        keywordRule "break"     ReservedKeyword,
        keywordRule "continue"  ReservedKeyword,
        keywordRule "loop"      ReservedKeyword,
        keywordRule "import"    ReservedKeyword,
        keywordRule "module"    ReservedKeyword,
        keywordRule "is"        ReservedKeyword,
        keywordRule "with"      ReservedKeyword,
        keywordRule "when"      ReservedKeyword,
        keywordRule "infix"     ReservedKeyword,
--         keywordRule "yield"     ReservedKeyword,    -- coroutines?
--         keywordRule "block"     ReservedKeyword,    -- coroutines?
--         keywordRule "launch"    ReservedKeyword,    -- coroutines?
--         keywordRule "join"      ReservedKeyword,    -- coroutines?
        keywordRule "match"     ReservedKeyword,
        keywordRule "case"      ReservedKeyword,
        keywordRule "of"        ReservedKeyword,
        keywordRule "inline"    ReservedKeyword,
        keywordRule "noinline"  ReservedKeyword,
        keywordRule "try"       ReservedKeyword,
        keywordRule "catch"     ReservedKeyword,
        keywordRule "finally"   ReservedKeyword,
        keywordRule "throw"     ReservedKeyword,
        keywordRule "asm"       ReservedKeyword,
        keywordRule "static"    ReservedKeyword,
-- primitive types have the exact same rules as keywords, so put them next
        primTypeRule "i8"       PrimI8,
        primTypeRule "i16"      PrimI16,
        primTypeRule "i32"      PrimI32,
        primTypeRule "i64"      PrimI64,
        primTypeRule "u8"       PrimU8,
        primTypeRule "u16"      PrimU16,
        primTypeRule "u32"      PrimU32,
        primTypeRule "u64"      PrimU64,
        primTypeRule "f32"      PrimF32,
        primTypeRule "f64"      PrimF64,
        primTypeRule "int"      PrimInt,
        primTypeRule "uint"     PrimUint,
-- multi-character symbols go next, so they don't get clobbered by single-character symbols
        symbolRule ">>="        GtGtEq,
        symbolRule "<<="        LtLtEq,
        
        symbolRule ".."         DotDot,
        symbolRule "!!"         BangBang,
        symbolRule "::"         ColonColon,
        symbolRule ">>"         GtGt,
        symbolRule "<<"         LtLt,
        symbolRule ">="         GtEq,
        symbolRule "<="         LtEq,
        symbolRule "->"         RightArrow,
        symbolRule "=>"         DoubleRightArrow,
        symbolRule "&&"         AndAnd,
        symbolRule "||"         OrOr,
        symbolRule "=="         EqEq,
        symbolRule "!="         NEq,
        symbolRule "*="         MultEq,
        symbolRule "+="         PlusEq,
        symbolRule "-="         MinusEq,
        symbolRule "&="         AndEq,
        symbolRule "|="         OrEq,
        symbolRule "^="         CarrotEq,
        symbolRule "/="         DivEq,
        symbolRule "%="         ModEq,
        
        symbolRule "."          Dot,
        symbolRule "-"          Minus,
        symbolRule "~"          Tilde,
        symbolRule "!"          Bang,
        symbolRule "?"          Question,
        symbolRule ";"          SemiColon,
        symbolRule "{"          OpenCurly,
        symbolRule "}"          CloseCurly,
        symbolRule "("          OpenParen,
        symbolRule ")"          CloseParen,
        symbolRule "["          OpenSquare,
        symbolRule "]"          CloseSquare,
        symbolRule "<"          Lt,
        symbolRule ">"          Gt,
        symbolRule ":"          Colon,
        symbolRule ","          Comma,
        symbolRule "+"          Plus,
        symbolRule "*"          Star,
        symbolRule "/"          Div,
        symbolRule "^"          Carrot,
        symbolRule "&"          And,
        symbolRule "="          Equ,
        symbolRule "|"          Or,

-- identifiers and numerical literals.
        identifierRule,
        
        newLineRule,
-- HACK: eliminates whitespace. horribly inefficient.
        symbolRule " "          Comment,
        symbolRule "\r"         Comment,
        symbolRule "\t"         Comment
        ]

tokenize :: TokenParse -> [ParseRule] -> [TokenParse]
tokenize (Token lno t, s) r
    | s == []   = [(Token lno t,s), (Token lno EndMod, s)]
    | otherwise = case parse of
                        [] -> error $ "\n\ntokenizer error near: " ++ (takeWhile (/='\n') s)
                        ((Token n x,y):xs) -> (Token n t, take 24 s) : tokenize (Token n x,y) r
    where parse = [x | Just x <- map ($ lno) $ map ($ s) $ r]

tokenizeModule :: String -> [Token]
tokenizeModule s =
    filter (\(Token a c) -> c /= Comment)
        $ map (\(x, y) -> x)
        $ tokenize (Token 1 BeginMod, s ++ " ") rules_round1


-- main = readFile "simple.dr" >>= (print . tokenizeModule)
