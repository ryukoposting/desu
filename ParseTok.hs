module ParseTok (
    Namespace, Identifier(..), Sentence(..), Expr(..),
    Type(..), PTData(..), ParseTok(..),
    token, anytok, ignore, scan, between, maybetween, perror,
    tokpeek, firstof, _id, namespace, emptynamespace, trytok,
    basicErr, parseErr, parsetok, varexpr, (<|>), ignoreToken
) where

import Tokenizer
import ErrMsg
import Utils

import Data.Either
import Data.Maybe (catMaybes)
import Data.List (intercalate)
import Data.List.Split (split, endsWith)
import Control.Applicative
import Control.Monad (liftM2)

-- type ErrMsg = String
type Namespace = [String]

data Identifier = Identifier (Namespace) (Token) deriving Show

-- AST-related declaration
data Sentence = Expr (Expr)     -- return a value
              | Macro           -- meta
              deriving Show

data Expr                    -- vvvvvvvvvv elif/else chaining
    = ExprParen (Expr)
    | ExprIf (Expr) (Expr) (Expr)
    | ExprElse [Expr]
    | ExprLitInt (Integer)
    | ExprLitFloat (Float)
    | ExprLitStr (String)
    | ExprLitChar (Char)
    | ExprVariable (Identifier)
    | ExprProcedure [Expr]
    | ExprBinOp (Expr) (Token) (Expr)
    | ExprUnqualDef (Identifier) (Type)
    | ExprVarDef (Identifier) (Type) (Expr)
    | ExprLetDef (Identifier) (Type) (Expr)
    | ExprFunDef (Identifier) (Type) [Identifier] (Expr)
    | ExprFnCall (Identifier) [Expr]
    | ExprVoid
    deriving Show

-- Type system-related declarations
data Type
    = TypeStruct (Identifier)   -- TODO: pack with generic info
    | TypeUnion (Identifier)    -- TODO: pack with generic info
    | TypePrimitive (Token)
    | TypeArray (Type) (Expr)
    | TypeFunction [Type] (Type)
    | TypeTuple [Type]
    | TypeResolveMe (Identifier)
    | TypeParametric (Type) [Type]
    | TypePointer (Type)
    | TypeUnspec
    deriving Show

newtype ParseTok a = PT ([Token] -> PTData a)

data PTData a = Ok (a, [Token])
              | Err [ErrMsg]
              deriving Show

basicErr s = Err $ [BasicError s]

parseErr t s = Err $ [ParseError t s]

parsetok :: ParseTok a -> [Token] -> PTData a
parsetok (PT p) ts = p ts

perror :: String -> ParseTok a
perror s = PT (\inp -> case inp of
                            [] -> basicErr s
                            (x:_) -> parseErr x s)

instance Functor ParseTok where
    fmap g p = PT (\inp -> case parsetok p inp of
                                Err s -> Err s
                                Ok (v, o) -> Ok (g v, o))

instance Applicative ParseTok where
    -- pure :: a -> Parser a
    pure v = PT (\inp -> Ok (v, inp))
    
    -- if pg fails, stop there,
    -- if pg succeeds, run px on pg's output.
    -- ParseTok (a -> b) -> ParseTok a -> ParseTok b
    pg <*> px = PT (\inp -> case parsetok pg inp of
                                 Err s -> Err s
                                 Ok (g, out) -> parsetok (fmap g px) out)

instance Monad ParseTok where
    return = pure
    p >>= f = PT (\inp -> case parsetok p inp of
                            Err s -> Err s
                            Ok (v, o) -> parsetok (f v) o)
    fail s = perror s

instance Alternative ParseTok where
    empty = perror "unspecified error!"
    (<|>) = (<++)

PT fa <++ PT fb = PT (\k ->
    case fa k of
         Err e -> case fb k of
                       Err f -> Err $ f ++ (take 1 e)
                       _ -> fb k
         _ -> fa k)


-- match and consume any token. Fails on an empty input.
anytok :: ParseTok Token
anytok = PT (\inp -> case inp of
                         [] -> basicErr "Expected a token"
                         (x:xs) -> Ok (x,xs))



-- don't consume anything, and spit out a non-error result.
-- used for type inference and unqualified types.
ignore :: a -> ParseTok a
ignore x = return x

ignoreToken :: TokenKey -> ParseTok Token
ignoreToken k = return $ Token (-1) k

-- parse a until you find end, then stop. Fails if a fails before end.
scan :: ParseTok a -> ParseTok end -> ParseTok [a]
scan p end = scan
    where scan = (end >> return []) <++ (liftM2 (:) p scan)

between :: Applicative m => m bra -> m ket -> m a -> m a
between bra ket p = bra *> p <* ket

maybetween :: TokenKey -> TokenKey -> ParseTok a -> ParseTok a
maybetween bra ket p = (token "Expected statement between brackets" bra *> p <* token "Expected statement between brackets" ket) <|> p



token :: String -> TokenKey -> ParseTok Token
token s x = do 
    (Token lno c) <- anytok
    if x == c
        then return $ Token lno c
        else fail $ s ++ " (expected token: " ++ (show x) ++ ")"

tokpeek :: TokenKey -> ParseTok ()
tokpeek i = PT (\ts -> case ts of
                          [] -> basicErr $ "Token mismatch. Found no tokens. Expected token " ++ (show i)
                          (Token lno tk:xs) -> if tk == i
                                                  then Ok ((), (Token lno tk:xs))
                                                  else parseErr (Token lno tk)
                                                      $ "Token mismatch. Expected token " ++ (show i))

trytok :: TokenKey -> ParseTok Bool
trytok i = PT (\ts -> case ts of
                          [] -> Ok (False, [])
                          (Token lno tk:xs) -> Ok (tk == i, xs))

firstof :: [TokenKey] -> ParseTok Token
firstof xs = PT (\tts -> case tts of
                            [] -> basicErr $ "No tokens left where one of the following was expected: " ++ (show xs)
                            (Token lno tk:ts) -> if elemr xs tk
                                                    then Ok (Token lno tk, ts)
                                                    else parseErr (Token lno tk)
                                                        $ "Token mismatch. Any one of the following was expected: " ++ (show xs))


_id :: String -> ParseTok Token
_id s = PT (\ts -> case ts of
                    [] -> basicErr s
                    (Token ln (Id a):xs) -> Ok (Token ln $ Id a, xs)
                    (e:_) -> parseErr e s)

-- tries to parse a namespace, fails if it can't find one.
namespace :: ParseTok Namespace
namespace = PT (\ts -> case ts of
                            [] -> basicErr "Expected namespace"
                            (Token ln (Id a):Token i ColonColon:xs) -> Ok (parseNamespace (Token ln (Id a):Token i ColonColon:xs))
                            (e:_) -> parseErr e "Expected namespace")
    where 
        parseNamespace []     = ([],[])
        parseNamespace (x:[]) = ([],[x])
        parseNamespace (x:y:xs) = case (x, y) of
                                    (Token _ (Id a), Token _ ColonColon) -> combine [a] $ parseNamespace xs
                                    (_, _) -> ([], (x:y:xs))
        combine = \a (nss, ts) -> (a ++ nss, ts)

-- always succeeds, giving an empty namespace
emptynamespace = PT (\xs -> Ok ([],xs))


-- an expression of a variable. NOT a declaration of one!
varexpr :: ParseTok Expr
varexpr = do
    n <- namespace <|> emptynamespace
    i <- _id "Expected variable name"
    return $ ExprVariable (Identifier n i)

-- aggregate type declaration general rule
aggrdecl :: (Identifier -> Type) -> TokenKey -> ParseTok Type
aggrdecl c t = do
    let tok = token $ "In aggregate type (" ++ (show t) ++ ")"
    tok t
    n <- namespace <|> emptynamespace
    i <- _id "No identifier given for aggregate type"
    return $ c (Identifier n i)

structdecl = aggrdecl TypeStruct Struct
uniondecl = aggrdecl TypeUnion Union



--  DEFINITION/DECLARATION PARSERS  --

-- fun parameter list


