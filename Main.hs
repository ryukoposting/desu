module Main where

import Tokenizer
import ParseTok
import TypeParsers
import LiteralParsers

-- firstof (x:xs) = do c <- token x <|> firstof xs
--                     return c

-- parser builder for all literals

funparams :: ParseTok [(Identifier,Type)]
funparams = do
    let tok = token "funparams"
    let arg = do
            name <- _id "Missing variable name"
            tok Colon
            typ <- explicittype
            return (Identifier [] name, typ)
    p <- arg
    ps <- scan (tok Comma *> arg) (tok CloseParen)
    return (p:ps)

fundef :: ParseTok Expr
fundef = do
    let tok = token "Syntax error in function definition"
    tok Fun
    ns <- namespace <|> emptynamespace
    name <- (_id "Expected identifier for function declaration")
    tok OpenParen
    args <- funparams <|> (tok CloseParen *> return [])
    outtype <- (tok Colon *> explicittype) <|> ignore TypeUnspec
    proc <- (tok Equ *> evalexpr) <|> procedure --TODO
    return $ ExprFunDef (Identifier [] name)
                        (TypeFunction (map snd args) outtype)
                        (map fst args) proc

anonymousfundef :: ParseTok Expr
anonymousfundef = do
    let tok = token "Syntax error in anonymous function definition"
    Token lno _ <- tok Fun
    tok OpenParen
    args <- funparams <|> (tok CloseParen *> return [])
    outtype <- (tok Colon *> explicittype) <|> ignore TypeUnspec
    proc <- (tok Equ *> evalexpr) <|> procedure --TODO
    return $ ExprFunDef (Identifier [] $ Token lno $ Id "anonymous")
                        (TypeFunction (map snd args) outtype)
                        (map fst args) proc

vardef :: ParseTok Expr
vardef = do
    token "Did not find 'let' keyword" Var
    name <- _id "Missing name for var-keyworded variable"
    typ <- (token "" Colon *> explicittype) <|> ignore TypeUnspec
    expr <- (token "Var expressions require initialization" Equ) *> evalexpr
    return $ ExprVarDef (Identifier [] name) typ expr

letdef :: ParseTok Expr
letdef = do
    token "Did not find 'let' keyword" Let
    name <- _id "Missing name for let-keyworded variable"
    typ <- (token "" Colon *> explicittype) <|> ignore TypeUnspec
    expr <- (token "Let expressions require initialization" Equ) *> evalexpr
    return $ ExprLetDef (Identifier [] name) typ expr
                
-- mutdef = qualdef MutDef Mut $ ignore Nothing
-- refdef = qualdef RefDef Ref $ ignore Nothing

--  HIGHER-LEVEL EXPRESSION PARSERS  --
infixexpr :: ParseTok Expr
infixexpr = do
    let exprs = varexpr <|> ifexpr <|> anylit <|> anylit
    left <- exprs <|> (token "" OpenParen *> infixexpr <* token "" CloseParen)
    oper <- firstof [DotDot, GtGt, LtLt, EqEq, GtEq, LtEq, AndAnd, OrOr, NEq,
                     Minus, Lt, Gt, Plus, Star, Div, Carrot, And, Or]
    right <- exprs <|> infixexpr
    return $ ExprBinOp left oper right

ifexpr :: ParseTok Expr
ifexpr = do
    let tok = token "Error while parsing if statement"
    tok If
    expr <- evalexpr
    result <- procedure <|> evalexpr
    els <- (tok Else *> (procedure <|> evalexpr)) <|> ignore ExprVoid
    return $ ExprIf expr result els

evalexpr :: ParseTok Expr
evalexpr = do
    let exprs = infixexpr <|> anonymousfundef <|> fncall <|> varexpr <|> ifexpr <|> anylit
    i <- (token "missing open parentheses" OpenParen *> exprs <* token "missing close parentheses" CloseParen) <|> exprs
    return i

fncall :: ParseTok Expr
fncall = do
    let tok = token "Function call"
    ExprVariable v <- varexpr
    tok OpenParen
    p <- evalexpr <|> ignore ExprVoid
    ps <- case p of
                ExprVoid -> (ignore [] <* (tok CloseParen))
                _ -> scan evalexpr (tok CloseParen)
    return $ ExprFnCall v $ filter (\x -> case x of
                                               ExprVoid -> False
                                               _ -> True) (p:ps)
    

procedure :: ParseTok Expr
procedure = do
    let tok = token "procedure"
    tok OpenCurly
    let procexprs = do
        p <- letdef <|> vardef
        return p
    exprs <- scan procexprs $ tok CloseCurly
    return $ ExprProcedure exprs

test = do
    let tok = token "test"
    tok BeginMod
    o <- scan fundef (tok EndMod)
    return o
    
prettifyParser :: PTData [Expr] -> String
prettifyParser ptdata = case ptdata of
                             Ok xs -> unlines (map show $ fst xs)
                             Err es -> unlines (map show $ es)

main = readFile "simple.dr" >>= (putStrLn . prettifyParser . (parsetok test) . Tokenizer.tokenizeModule)
