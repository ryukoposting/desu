module TypeParsers (
    explicittype,
    optionaltype
) where

import Tokenizer
import ParseTok
import LiteralParsers
import Utils

explicittype :: ParseTok Type
explicittype = PT(\tts -> let tps = [prim, genericimpltype, tupletype, lambdatype, resolvemetype, pointertype] in
                             case tts of
                                  [] -> basicErr "Expected an explicit type, found EOF"
                                  (t:ts) -> case [r | Ok(r) <- map ($ (t:ts)) $ map parsetok tps] of
                                                [] -> parseErr t "Expected an explicit type here"
                                                (m:_) -> Ok(m))


optionaltype :: ParseTok Type
optionaltype = explicittype <|> ignore TypeUnspec

prim :: ParseTok Type
prim = PT (\ts -> case ts of
                    [] -> basicErr "Expected type"
                    (Token lno x:xs) -> if elemr [PrimI8, PrimI16, PrimI32, PrimI64,
                                                  PrimU8, PrimU16, PrimU32, PrimU64,
                                                  PrimF32, PrimF64,
                                                  PrimUint, PrimInt] x
                                                  then Ok (TypePrimitive (Token lno x), xs)
                                                  else parseErr (Token lno x) "Invalid specifier for primitive type")

arrtype :: ParseTok Type
arrtype = do
    let tok = token "Array Type"
    tok OpenSquare
    t <- explicittype
    tok Colon
    l <- intlit
    tok CloseSquare
    return $ TypeArray t l

lambdatype :: ParseTok Type
lambdatype = do
    let tok = token "Function Type"
    let more = do
            tok Comma
            out <- explicittype
            return out
    tok OpenParen
    inn <- explicittype
    ins <- scan more (tok RightArrow)
    out <- explicittype
    tok CloseParen
    return $ TypeFunction (inn:ins) out

genericimpltype :: ParseTok Type
genericimpltype = do
    let tok = token "Generic Type"
    let more = do
            tok Comma
            out <- explicittype
            return out
    n <- resolvemetype
    tok OpenSquare
    p <- explicittype
    ps <- scan more (tok CloseSquare)
    return $ TypeParametric n (p:ps)

tupletype :: ParseTok Type
tupletype = do
    let tok = token "Tuple Type"
    let more = do
            tok Comma
            out <- explicittype
            return out
    tok OpenParen
    p <- explicittype
    ps <- scan more (tok CloseParen)
    return $ TypeTuple (p:ps)

resolvemetype :: ParseTok Type
resolvemetype = do
    n <- namespace <|> emptynamespace
    i <- _id "no match for type identifier"
    return $ TypeResolveMe $ Identifier n i

pointertype :: ParseTok Type
pointertype = do
    let tok = token "Pointer Type"
    tok Star
    t <- explicittype
    return $ TypePointer t
